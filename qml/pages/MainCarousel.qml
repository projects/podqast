import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    SilicaFlickable {
        id: mainFlickable
        anchors {
            fill: parent
        }
        flickableDirection: Flickable.VerticalFlick

        RefreshPodcastsPulley {
            id: menu
        }

        TabHeader {
            id: mainPageHeader
            listView: swipeView
            indicatorOnTop: false

            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
            }
            iconArray: ["image://theme/icon-m-downloads", "image://theme/icon-m-media-songs", "image://theme/icon-m-company"]
            textArray: [qsTr("Inbox"), qsTr("Playlist"), qsTr("Library")]
        }

        SlideshowView {
            id: swipeView
            itemWidth: width
            itemHeight: height
            orientation: Qt.Horizontal

            anchors.top: mainPageHeader.bottom
            anchors.topMargin: Theme.paddingLarge
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: pdp.top

            property var carouselPages: ["Inbox.qml", "Queue.qml", "Archive.qml"]
            property int initialPage: 1
            model: carouselPages.length
            Component.onCompleted: currentIndex = initialPage

            delegate: Loader {
                width: swipeView.itemWidth
                height: swipeView.height
                source: swipeView.carouselPages[index]
                asynchronous: true
            }
        }

        PlayDockedPanel {
            id: pdp
        }
        PushUpMenu {
            MenuItem {
                text: qsTr("Settings")
                onClicked: pageStack.push(Qt.resolvedUrl(
                                              "../pages/Settings.qml"))
            }
            MenuItem {
                text: qsTr("Log")
                onClicked: pageStack.push(Qt.resolvedUrl("../pages/Log.qml"))
            }
            MenuItem {
                text: qsTr("About")
                onClicked: pageStack.push(Qt.resolvedUrl("About.qml"))
            }
        }
    }
}
