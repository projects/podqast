import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Item {
    id: page
    Component.onCompleted: {
        queuehandler.getQueueEntries()
        if (doDownloadConf.value && (wifiConnected || doMobileDownConf.value)) {
            queuehandler.downloadAudioAll()
        }
    }

    Connections {
        target: queuehandler
        ignoreUnknownSignals: true
        onCreateList: {
            queuePostModel.clear()
            for (var i = 0; i < data.length; i++) {
                queuePostModel.append(data[i])
            }
            if (i > 0) {
                playeropen = true
                playerHandler.podcast_logo = data[0].podcast_logo
            } else {
                playeropen = false
            }
        }
    }

    Connections {
        target: feedparserhandler
        onRefreshFinished: {
            queuehandler.getQueueEntries()
        }
    }

    SilicaListView {
        id: queuepostlist
        clip: true
        anchors.fill: parent
        spacing: Theme.paddingMedium

        //prevents that inserting an item into the queue opens the menu as well
        // (this overlays on the other pages in the MainCarousel)
        property bool postMoveTriggered: false

        model: ListModel {
            id: queuePostModel
        }
        delegate: QueuePostListItem {}
        ViewPlaceholder {
            enabled: queuePostModel.count === 0
            text: qsTr("No posts")
            hintText: qsTr("Pull down to Discover new podcasts or get posts from Inbox or Library")
        }
    }
}
