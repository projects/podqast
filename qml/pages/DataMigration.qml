import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {

    id: migrationpage
    property bool running: false
    property bool errorHappened: false
    property var errorText: ""

    Connections {
        target: migrationhandler
        onMigrationDone: {
            podqast.isInitialized = true
            appNotification.previewSummary = qsTr("Update done")
            appNotification.previewBody = qsTr(
                        "Successfully migrated data to the current version")
            appNotification.body = appNotification.previewBody
            appNotification.publish()
            pageStack.replace(Qt.resolvedUrl("MainCarousel.qml"))
        }
        onMigrationStart: {
            console.log("migration started with steps: " + miglen)
            running = true
            migbar.maximumValue = 10000
            migbar.value = migrationhandler.progress * migbar.maximumValue
        }

        onMigrationProgress: {
            migbar.value = migrationhandler.progress * migbar.maximumValue
        }

        onError: {
            errorHappened = true
            errorText = traceback
        }
    }
    PageHeader {
        id: header
        title: qsTr("Data Migration")
    }
    SilicaFlickable {
        anchors {
            leftMargin: Theme.horizontalPageMargin
            rightMargin: Theme.horizontalPageMargin
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            top: header.bottom
        }
        clip: true
        contentHeight: migrationcontent.height + (2 * Theme.paddingMedium)

        VerticalScrollDecorator {}

        Column {
            id: migrationcontent
            width: parent.width
            anchors.top: header.bottom
            Column {
                id: migrationcomponent
                width: parent.width
                clip: true
                spacing: Theme.paddingLarge
                visible: !errorHappened

                Label {
                    width: parent.width
                    wrapMode: Text.WordWrap
                    visible: !running
                    //: general text explaining what the migration does
                    text: qsTr("Because the data format changed, podqast will need to migrate your data to the new format. You can use one of the buttons below to create a backup of your data for recovery.")
                }

                Label {
                    width: parent.width
                    wrapMode: Text.WordWrap
                    //: whatsnew section of the migration
                    text: qsTr("<b>Whats New?</b><ul><li>Some of you lost episodes in the last migration. Because we found this pretty sad, we're trying to recover them now.</li><li>If you are missing old episodes of feeds in general and this didn't help, try the force refresh button in the settings (We added support for Pagination).</li><li>Podcasts are now sorted alphabetically, we hope you like that!</li><li>Gpodder should work again.</li><li>Check the new Log Page next to the settings.</li>" + "<li>If you're streaming an episode and the download finishes, we're now switching to the downloaded file automatically.</li><li>Also more speed, bugfixes and code cleanups.</li></ul><br>If you want to contribute to podQast, you can help translating the app or report issues on GitLab.")
                }

                Label {
                    width: parent.width
                    wrapMode: Text.WordWrap
                    visible: running
                    text: qsTr("The migration is running, please wait.")
                }

                BackupButtons {
                    id: backupcomponent
                    visible: !running
                    width: parent.width
                    opmlButtonVisible: false
                    doBackupStore: true
                }

                Button {
                    text: qsTr("Start Migration")
                    onClicked: migrationhandler.start_migration(
                                   !strictmodeswitch.checked)
                    visible: !running
                    enabled: !backupcomponent.backupRunning
                             && !backupcomponent.opmlWriteRunning
                    width: parent.width
                    backgroundColor: Theme.highlightBackgroundColor
                }

                ProgressBar {
                    id: migbar
                    width: parent.width
                    height: Theme.itemSizeSmall
                    minimumValue: 1
                    visible: running
                }

                Item {
                    height: Theme.paddingMedium
                    width: parent.width
                }

                TextSwitch {
                    id: strictmodeswitch
                    visible: !running
                    text: qsTr("Try to ignore errors during migration")
                }
            }

            Column {
                id: errorcomponent
                visible: errorHappened
                spacing: Theme.paddingLarge

                Label {
                    width: parent.width
                    wrapMode: Text.WordWrap
                    text: qsTr("We couldnt migrate your data completly. You can restart the app and try it in the non-strict mode. Please consider reporting this as a bug in the Issue-Tracker.")
                }

                Row {
                    spacing: Theme.paddingSmall
                    Button {
                        text: qsTr("Copy to clipboard")
                        onClicked: Clipboard.text = errorText
                    }

                    Button {
                        text: qsTr("Open issue tracker")
                        backgroundColor: Theme.highlightBackgroundColor
                        onClicked: Qt.openUrlExternally(
                                       "https://gitlab.com/cy8aer/podqast/-/issues/new")
                    }
                }

                TextArea {
                    id: errortextarea
                    readOnly: true
                    wrapMode: Text.Wrap
                    text: errorText
                    width: parent.width
                    font.pixelSize: Theme.fontSizeSmall
                }
            }
        }
    }
}
