import QtQuick 2.0
import Sailfish.Silica 1.0
import "../lib/hints"
import "../components"
import "../components/hints"

Item {
    id: page

    Component.onCompleted: {
        feedparserhandler.getPodcasts()
    }

    SilicaListView {
        id: podcasts
        clip: true
        anchors.fill: parent
        section.property: "bethead"
        section.delegate: sectionTile
        VerticalScrollDecorator {}

        Component {
            id: sectionTile
            Rectangle {
                width: parent.width
                height: Theme.itemSizeExtraSmall / 10
                color: Theme.highlightDimmerColor
            }
        }

        Connections {
            target: feedparserhandler
            onOpmlImported: feedparserhandler.getPodcasts()
            onSubscribed: feedparserhandler.getPodcasts()
            onUnsubscribed: feedparserhandler.getPodcasts()

            onPodcastsList: {
                console.log("Data length: " + pcdata.length)
                podcastsModel.clear()
                podcastsModel.append({
                                         "bethead": "00title",
                                         "url": "",
                                         "description": "",
                                         "logo_url": "image://theme/icon-m-developer-mode",
                                         "title": qsTr("Manage Podcasts"),
                                         "topage": "Discover.qml"
                                     })
                podcastsModel.append({
                                         "bethead": "00title",
                                         "url": "",
                                         "description": "",
                                         "logo_url": "image://theme/icon-m-backup",
                                         "title": qsTr("History"),
                                         "topage": "History.qml"
                                     })
                podcastsModel.append({
                                         "bethead": "00title",
                                         "url": "",
                                         "description": "",
                                         "logo_url": "image://theme/icon-m-favorite-selected",
                                         "title": qsTr("Favorites"),
                                         "topage": "Favorites.qml"
                                     })
                if (allowExtConf.value) {
                    podcastsModel.append({
                                             "bethead": "0ext",
                                             "url": "",
                                             "description": "",
                                             "logo_url": "image://theme/icon-m-device-upload",
                                             "title": qsTr("External Audio"),
                                             "topage": "External.qml"
                                         })
                }
                for (var i = 0; i < pcdata.length; i++) {
                    pcdata[i]["bethead"] = "Podcast"
                    podcastsModel.append(pcdata[i])
                }
            }
        }

        ViewPlaceholder {
            enabled: podcastsModel.count == 0
            text: qsTr("Rendering")
            hintText: qsTr("Collecting Podcasts")
        }

        model: ListModel {
            id: podcastsModel
        }
        delegate: PodcastItem {}
    }

    SingleTimeHint {
        key: "/apps/ControlPanel/podqast/hints/archive_hint"
        hintComponent: "../../components/hints/MainPageUsageHint.qml"
    }
}
