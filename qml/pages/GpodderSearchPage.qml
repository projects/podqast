import QtQuick 2.0

PodcastDirectorySearchPage {
    onDoSearch: gpodderhandler.doSearch(query)

    Connections {
        target: gpodderhandler
        onPodSearch: {
            console.debug("Got " + podcasts.length + " results for the query")
            queryRunning = false
            podcastModel.clear()
            for (var i = 0; i < podcasts.length; i++) {
                if (i < 30) {
                    podcastModel.append(podcasts[i])
                }
            }
        }
    }
}
