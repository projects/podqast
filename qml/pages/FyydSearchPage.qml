import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "../components"

PodcastDirectorySearchPage {

    Connections {
        target: fyydhandler
        onPodSearch: {
            queryRunning = false
            podcastModel.clear()
            for (var i = 0; i < podcasts.length; i++) {
                if (i < 30) {
                    podcastModel.append(podcasts[i])
                }
            }
        }
    }
    onDoSearch: fyydhandler.doSearch(query)
}
