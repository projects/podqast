import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    property var episodeid
    property var chapterid

    property bool currentlyPlayingEpisode: (episodeid === playerHandler.firstid
                                            && playerHandler.isPlaying === true)

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Component.onCompleted: {
        chapterModel.clear()
        queuehandler.getEpisodeChapters(episodeid)
    }

    Connections {
        target: queuehandler

        onEpisodeChapters: {
            for (var i = 0; i < chapters.length; i++) {
                console.log("got chapter", i, chapters[i].selected,
                            chapters[i].title)
                chapterModel.append(chapters[i])
            }
        }
    }

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        SilicaListView {
            id: chapterslist
            clip: true
            anchors.top: parent.top
            width: parent.width
            height: page.height

            header: PageHeader {
                title: qsTr("Chapters")
            }

            model: ListModel {
                id: chapterModel
            }

            delegate: ListItem {
                id: listiitem

                contentHeight: textswitch.height
                width: page.width

                TextSwitch {
                    id: textswitch
                    width: page.width - jumptochapterbutton.width
                    text: title
                    description: msToTime(start_millis)
                    checked: selected
                    busy: (currentlyPlayingEpisode
                           && playerHandler.aktchapter === index)
                    onClicked: {
                        queuehandler.toggleChapter(episodeid, chapterid = index)
                        console.log(episodeid, playerHandler.firstid,
                                    playerHandler.isPlaying)
                        if (currentlyPlayingEpisode) {
                            console.log("changed chapter selection of currently playing episode")
                            queuehandler.sendFirstEpisodeChapters(episodeid)
                        }
                    }

                    function msToTime(s) {
                        function pad(n, z) {
                            z = z || 2
                            return ('00' + n).slice(-z)
                        }
                        var ms = s % 1000
                        s = (s - ms) / 1000
                        var secs = s % 60
                        s = (s - secs) / 60
                        var mins = s % 60
                        var hrs = (s - mins) / 60

                        return pad(hrs) + ':' + pad(mins) + ':' + pad(secs)
                    }
                }

                IconButton {
                    id: jumptochapterbutton
                    visible: textswitch.checked && currentlyPlayingEpisode
                    anchors.left: textswitch.right
                    icon.source: "image://theme/icon-m-next"
                    onClicked: {
                        var millis = start_millis
                        playerHandler.seek(millis)
                    }
                }
            }
            ViewPlaceholder {
                text: qsTr("No chapters")
                hintText: qsTr("Rendering chapters")
            }

            VerticalScrollDecorator {}
        }
    }
}
