import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Item {
    id: page

    Component.onCompleted: {
        inboxhandler.getInboxEntries("home")
    }

    Connections {
        target: inboxhandler
        onInboxData: {
            if (offset === 0) {
                inboxPostModel.clear()
            }
            for (var i = 0; i < data.length; i++) {
                inboxPostModel.append(data[i])
            }
        }
        onGetInboxPosts: {
            inboxhandler.getInboxEntries(podqast.ifilter)
        }
    }

    Connections {
        target: feedparserhandler
        onRefreshFinished: {
            inboxhandler.getInboxEntries(podqast.ifilter)
        }
    }

    SilicaListView {
        id: inboxpostlist
        clip: true
        anchors.fill: parent
        section.property: 'section'
        section.delegate: SectionHeader {
            text: section
            horizontalAlignment: Text.AlignRight
        }
        spacing: Theme.paddingMedium

        header: ListItem {
            contentHeight: Theme.itemSizeMedium
            visible: inboxPostModel.count > 0

            onClicked: {
                Remorse.popupAction(page, qsTr("Moving all posts to archive"),
                                    function () {
                                        inboxhandler.moveAllArchive()
                                    })
            }

            Item {
                anchors.fill: parent
                Icon {
                    anchors {
                        left: parent.left
                        leftMargin: (Theme.iconSizeLarge - Theme.iconSizeMedium) / 2
                        verticalCenter: parent.verticalCenter
                    }

                    width: Theme.iconSizeMedium
                    height: width
                    source: "image://theme/icon-m-backup"
                }
                Label {
                    anchors {
                        right: parent.right
                        rightMargin: Theme.paddingLarge
                    }

                    font.pixelSize: Theme.fontSizeMedium
                    font.bold: true
                    color: Theme.primaryColor
                    text: qsTr("Clear Inbox")
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignRight
                }
            }
            Rectangle {
                anchors.bottom: parent.bottom
                width: parent.width
                height: Theme.itemSizeExtraSmall / 10
                color: Theme.highlightDimmerColor
            }
        }

        ViewPlaceholder {
            enabled: inboxPostModel.count == 0
            text: qsTr("No new posts")
            hintText: qsTr("Pull down to Discover new podcasts, get posts from Library, or play the Playlist")
        }

        model: ListModel {
            id: inboxPostModel
        }
        delegate: InboxPostListItem {}
    }
}
