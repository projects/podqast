import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page

    property int margin: 16

    PageHeader {
        id: header
        title: qsTr("Discover")
    }

    SilicaListView {
        anchors.top: header.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        property bool experimental: experimentalConf.value

        model: ListModel {
            id: menumodel
            ListElement {
                title: qsTr("Search on gpodder.net")
                nextpage: "GpodderSearchPage.qml"
            }
            ListElement {
                title: qsTr("Search on fyyd.de")
                nextpage: "FyydSearchPage.qml"
            }
            ListElement {
                title: qsTr("Tags...")
                nextpage: "DiscoverTags.qml"
            }
            ListElement {
                title: qsTr("Url...")
                nextpage: "DiscoverUrl.qml"
            }
            ListElement {
                title: qsTr("Import...")
                nextpage: "DiscoverImport.qml"
            }
            ListElement {
                title: qsTr("Export...")
                nextpage: "DiscoverExport.qml"
            }
        }

        delegate: ListItem {
            onClicked: pageStack.push(Qt.resolvedUrl(nextpage))
            Label {
                anchors.left: parent.left
                anchors.leftMargin: margin
                anchors.verticalCenter: parent.verticalCenter
                text: title
            }
            IconButton {
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                icon.source: "image://theme/icon-m-right"
                onClicked: pageStack.push(Qt.resolvedUrl(nextpage))
            }
        }
    }
}
