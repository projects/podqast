import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Component.onCompleted: {
        archivehandler.getArchiveEntries("home")
    }

    Connections {
        target: archivehandler
        ignoreUnknownSignals : true
        onHistoryData: {
            if(offset === 0)
                historyPostModel.clear()
            for (var i = 0; i < data.length; i++) {
                historyPostModel.append(data[i]);
            }
        }
    }

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        VerticalScrollDecorator { }
        PrefAboutMenu {}


        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        Column {
            id: archivetitle

            width: page.width

            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("History")
            }
        }

        SilicaListView {
            id: archivepostlist
            clip: true
            anchors.top: archivetitle.bottom
            width: parent.width
            height: page.height - pdp.height - archivetitle.height
            section.property: 'asection'
            section.delegate: SectionHeader {
                text: section
                horizontalAlignment: Text.AlignRight
            }
            ViewPlaceholder {
                enabled: historyPostModel.count == 0
                text: qsTr("Rendering")
                hintText: qsTr("Collecting Posts")
                verticalOffset: - archivetitle.height
            }

            model: ListModel {
                id: historyPostModel
            }
            delegate: ArchivePostListItem { }
        }

        PlayDockedPanel { id: pdp }
    }
}
