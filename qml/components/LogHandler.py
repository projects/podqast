import asyncio
from typing import Iterator
import pyotherside

from podcast.persistent_log import LogMessage, get_log_messages
from podcast.util import chunks


async def get_logs_asnyc():
    log: Iterator[LogMessage] = get_log_messages()
    for offset, max, chunk in chunks(log, 32):
        pyotherside.send("logData",offset, [log.to_dict() for log in chunk])

def get_logs():
    asyncio.run(get_logs_asnyc())