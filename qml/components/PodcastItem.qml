import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Thumbnailer 1.0

ListItem {
    id: podcastItem
    width: ListView.view.width
    contentHeight: topage ? Theme.itemSizeMedium : Theme.itemSizeLarge
    onClicked: if (topage) {
                   pageStack.push(Qt.resolvedUrl(("../pages/" + topage)))
               } else {
                   pageStack.push(Qt.resolvedUrl("../pages/PodpostList.qml"), {
                                      "url": url,
                                      "title": title
                                  })
               }

    menu: Component {
        IconContextMenu {
            visible: !topage
            enabled: !topage
            id: podcastContextMenu
            PodqastIconMenuItem {
                text: qsTr('Refresh')
                icon.source: 'image://theme/icon-m-refresh'
                onClicked: {
                    closeMenu()
                    feedparserhandler.refreshPodcast(url)
                }
            }
            PodqastIconMenuItem {
                text: qsTr('Settings')
                icon.source: 'image://theme/icon-m-developer-mode'
                onClicked: {
                    closeMenu()
                    pageStack.push(Qt.resolvedUrl(
                                       "../pages/PodcastSettings.qml"), {
                                       "podtitle": title,
                                       "url": url
                                   })
                }
            }
            PodqastIconMenuItem {
                text: qsTr('Delete')
                icon.source: 'image://theme/icon-m-delete'
                onClicked: {
                    var fph = feedparserhandler
                    var theurl = url
                    closeMenu()
                    Remorse.itemAction(podcastItem, qsTr("Deleting Podcast"),
                                       function () {
                                           fph.deletePodcast(theurl)
                                       })
                }
            }
        }
    }
    Image {
        id: listicon
        asynchronous: true
        anchors.left: parent.left
        width: topage ? Theme.iconSizeMedium : Theme.iconSizeLarge
        height: topage ? Theme.iconSizeMedium : Theme.iconSizeLarge
        sourceSize.width: topage ? Theme.iconSizeMedium : Theme.iconSizeLarge
        sourceSize.height: topage ? Theme.iconSizeMedium : Theme.iconSizeLarge
        anchors.margins: Theme.paddingMedium
        anchors.verticalCenter: parent.verticalCenter
        source: if (topage) {
                    logo_url
                } else {
                    logo_url == "" ? "../../images/podcast.png" : "image://nemoThumbnail/"
                                     + logo_url
                }
    }

    Label {
        id: titlelabel
        anchors.margins: Theme.paddingMedium
        anchors.left: listicon.right
        anchors.right: parent.right
        height: parent.height
        text: title
        font.pixelSize: Theme.fontSizeSmall
        font.bold: true
        wrapMode: Text.WordWrap
        truncationMode: TruncationMode.Elide
        verticalAlignment: Text.AlignVCenter
    }
}
