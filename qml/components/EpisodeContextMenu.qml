import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Thumbnailer 1.0

IconContextMenu {
    id: contextMenu

    property bool archiveEnabled
    property bool favoriteEnabled

    property var upPressedHandler: function (model) {
        queuehandler.queueInsertNext(model.id)
    }
    property var downPressedHandler: function (model) {
        queuehandler.queueInsertBottom(model.id)
    }

    property var archivePressedHandler
    spacing: (archiveEnabled
              && favoriteEnabled) ? Theme.paddingMedium : Theme.paddingLarge

    PodqastIconMenuItem {
        text: 'Play'
        icon.source: "image://theme/icon-m-" + (model.id === playerHandler.firstid
                                                && playerHandler.isPlaying ? "pause" : "play")
        onClicked: {
            if (model.id === playerHandler.firstid) {
                playerHandler.playpause()
            } else {
                queuehandler.queueInsertTop(model.id, function () {
                    playerHandler.play()
                })
            }
            closeMenu()
        }
    }
    PodqastIconMenuItem {
        text: 'QueueTop'
        icon.source: 'image://theme/icon-m-up'
        onClicked: {
            upPressedHandler(model)
            closeMenu()
        }
    }
    PodqastIconMenuItem {
        text: 'QueueBottom'
        icon.source: 'image://theme/icon-m-down'
        onClicked: {
            downPressedHandler(model)
            closeMenu()
        }
    }
    Loader {
        active: archiveEnabled
        sourceComponent: PodqastIconMenuItem {
            text: 'Archive'
            icon.source: 'image://theme/icon-m-backup'
            onClicked: {
                archivePressedHandler(model)
                closeMenu()
            }
        }
    }
    Loader {
        active: favoriteEnabled
        sourceComponent: PodqastIconMenuItem {
            id: favoriteicon
            text: 'Favorite'
            icon.source: 'image://theme/icon-m-favorite' + (isfavorite ? "-selected" : "")
            property bool toggeling: false
            onClicked: {
                console.debug("Toggling favorite for " + model.id)
                toggeling = true
                favoritehandler.toggleFavorite(model.id)
            }
            Connections {
                target: favoritehandler
                onFavoriteToggled: {
                    if (model.id === podpost) {
                        console.debug("favorite was toggled for " + model.id)
                        isfavorite = favstate
                        favoriteicon.toggeling = false
                    }
                }
            }
            BusyIndicator {
                anchors.centerIn: parent
                size: BusyIndicatorSize.Medium
                running: favoriteicon.toggeling
            }
        }
    }

    PodqastIconMenuItem {
        id: chapterselection
        text: qsTr("Select chapters")
        icon.source: 'image://theme/icon-m-menu'
        opacity: (haschapters) ? 1 : 0.3
        enabled: (haschapters)
        onClicked: {
            pageStack.push(Qt.resolvedUrl("../pages/Chapters.qml"), {
                               "episodeid": model.id
                           })
        }
    }
}
