import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Column {

    SearchField {
        id: episodeSearchField
        width: parent.width
        height: Theme.itemSizeMedium
        visible: false
    }

    Row {
        id: episodeSortField
        width: parent.width
        visible: false
        height: Theme.itemSizeMedium
        LayoutMirroring.enabled: true

        IconButton {
            icon.source: "image://theme/icon-s-duration"
        }

        IconButton {
            icon.source: "image://theme/icon-m-moon"
        }

        IconButton {
            icon.source: "image://theme/icon-m-mic-mute"
        }
    }

    Row {
        id: settingsrow
        height: Theme.itemSizeMedium
        width: parent.width
        visible: false
        LayoutMirroring.enabled: true

        IconButton {
            icon.source: "image://theme/icon-m-cancel"
            onClicked: {
                settingsrow.visible = false
            }
        }

        IconButton {
            id: prefsicon
            icon.source: "image://theme/icon-m-developer-mode"
            onClicked: {
                pageStack.push(Qt.resolvedUrl(
                                   "PodcastSettings.qml"), {
                                   "podtitle": title,
                                   "url": url
                               })
            }
        }

        IconButton {
            id: searchicon
            icon.source: "image://theme/icon-m-search"
            onClicked: {
                episodeSearchField.visible = !episodeSearchField.visible
            }
        }

        IconButton {
            id: sorticon
            icon.source: "image://theme/icon-m-transfer"
            onClicked: {
                episodeSortField.visible = !episodeSortField.visible
            }
        }
    }
}
