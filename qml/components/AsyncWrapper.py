import asyncio
import threading
from concurrent.futures import Executor
from functools import wraps

loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)


def async_threaded(pool: Executor = None):
    def wrapper(fun):
        @wraps(fun)
        async def inner(*args, **kwargs):
            coroutine = fun(*args, **kwargs)
            if pool is None:
                thread = threading.Thread(target=asyncio.run, args=(coroutine,))
                thread.start()
            else:
                pool.submit(asyncio.run, (coroutine,))
        return inner
    return wrapper


class AsyncWrapper(object):
    def __init__(self, subject):
        self.__subject = subject

    def __getattr__(self, item):
        fun = getattr(self.__subject, item)

        def wrapped(*args, **kwargs):
            asyncio.run(fun(*args, **kwargs))

        return wrapped
