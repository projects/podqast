import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: archivehandler

    signal historyData(int offset, var data)

    Component.onCompleted: {
        addImportPath(Qt.resolvedUrl("./python"))
        setHandler("historyData", historyData)

        addImportPath(Qt.resolvedUrl('.'))
        importModule('ArchiveHandler', function () {
            console.log('ArchiveHandler is now imported')
        })
    }

    onError: {
        console.log('python error: ' + traceback)
    }

    function getArchiveEntries(podurl) {
        if (podurl === "home") {
            call("ArchiveHandler.archivehandler.getarchiveposts",
                 function () {})
        } else {
            call("ArchiveHandler.archivehandler.getarchiveposts", [podurl],
                 function () {})
        }
    }
}
