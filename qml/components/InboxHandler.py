# -*- coding: utf-8 -*-
from typing import Iterator

import pyotherside
import threading
import sys

sys.path.append("/usr/share/harbour-podqast/python")
from podcast.util import chunks
from podcast.podpost import Podpost

from podcast.inbox import InboxFactory
from podcast.queue import QueueFactory


def get_inbox_posts(podurl=None):
    """
    Return a list of all inbox posts
    """

    posts: Iterator[Podpost] = InboxFactory().get_inbox().get_podposts_objects(podurl=podurl)

    invoked: bool = False
    for offset, max, chunk in chunks(posts, 32):
        invoked=True
        pyotherside.send("inboxData", offset, [post.get_data() for post in chunk])
    if not invoked:
        pyotherside.send("inboxData", 0, [])


def move_queue_top(podpost):
    """
    Move element to top of queue
    """

    InboxFactory().get_inbox().move_queue_top(podpost)
    pyotherside.send("getInboxPosts")


def move_queue_next(podpost):
    """
    Move element to next queue position
    """

    InboxFactory().get_inbox().move_queue_next(podpost)
    pyotherside.send("getInboxPosts")


def move_queue_bottom(podpost):
    """
    Move element to last queue position
    """

    InboxFactory().get_inbox().move_queue_bottom(podpost)
    pyotherside.send("getInboxPosts")


def move_archive(podpost):
    """
    Move post to archive
    """

    inbox = InboxFactory().get_inbox()
    inbox.move_archive(podpost)
    pyotherside.send("getInboxPosts")


def move_all_archive():
    """
    Move all posts to archive
    """

    inbox = InboxFactory().get_inbox()
    inbox.move_all_archive()
    pyotherside.send("getInboxPosts")


def queue_do_download(id):
    """
    Do the Download of an archive
    """

    queue = QueueFactory().get_queue()
    for perc in queue.download(id):
        pyotherside.send("downloading", id, perc)


class InboxHandler:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()

    def getinboxposts(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_inbox_posts)
        self.bgthread.start()

    def queuedownload(self, id):
        """
        download audio post
        """

        dlthread = threading.Thread(target=queue_do_download, args=[id])
        dlthread.start()

    def movequeuetop(self, podpost):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=move_queue_top, args=[podpost])
        self.bgthread.start()

    def movequeuenext(self, podpost):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=move_queue_next, args=[podpost]
        )
        self.bgthread.start()

    def movequeuebottom(self, podpost):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=move_queue_bottom, args=[podpost]
        )
        self.bgthread.start()

    def movearchive(self, podpost):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=move_archive, args=[podpost])
        self.bgthread.start()

    def moveallarchive(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=move_all_archive)
        self.bgthread.start()


inboxhandler = InboxHandler()
