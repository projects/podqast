import QtQuick 2.0
import Sailfish.Silica 1.0

Rectangle {
    width: Theme.iconSizeExtraLarge
    height: Theme.iconSizeExtraLarge
    anchors.centerIn: centeredComponent
    color: Theme.darkPrimaryColor
    Icon {
        property string streamIcon: "image://theme/icon-s-cloud-download"
        property string sleepTimerIcon: "image://theme/icon-s-timer"
        property string playrateIcon: "image://theme/icon-s-duration"
        property var sources: [streamIcon, sleepTimerIcon, playrateIcon]
        width: Theme.iconSizeExtraLarge
        height: Theme.iconSizeExtraLarge
        anchors.fill: centeredComponent
        property int baseindex: 4
        property int relativeIndex: index - baseindex
        source: sources[relativeIndex]

        onRelativeIndexChanged: console.log("relative index" + relativeIndex)
    }
}
