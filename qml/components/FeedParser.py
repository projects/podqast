# -*- coding: utf-8 -*-
import logging
import threading
from urllib.error import URLError
import urllib.parse

import pyotherside
import os
import time
import tarfile

from AsyncWrapper import AsyncWrapper, async_threaded
from podcast import persistent_log
from podcast.constants import Constants
from podcast.persistent_log import LogType
from podcast.podcast import PodcastFactory, Podcast
from podcast.podcastlist import PodcastListFactory
from podcast.archive import ArchiveFactory
from podcast.search import DictParser
from podcast.util import create_opml, movePost, chunks, MovePostToPage

logger = logging.getLogger(__name__)


def init_from_qml(object):
    Constants().init_from_qml(object)


# safeguard to prevent parallel execution
lock_refresh = threading.Lock()


class FeedParser:
    def __init__(self):
        pyotherside.atexit(self.doexit)

    async def get_feedinfo(self, url, preview=False, num_preview_episodes=3):
        logger.info("getting feedinfo %s for %s",
                    "in previewmode with %d episodes" % num_preview_episodes if preview else "", url)
        podcast = None
        episodes = None
        if not urllib.parse.urlparse(url).scheme:
            logger.warning("Adding https to schemaless url %s", url)
            url = 'https://' + url
        try:
            if preview:
                podcast, episodes = Podcast.create_from_url(url, preview=True, num_preview_items=num_preview_episodes)
            else:
                podcast = PodcastFactory().get_podcast(url)
                episodes = list(podcast.episodes.limit(num_preview_episodes)) if num_preview_episodes > 0 else []
        except URLError:
            pyotherside.send("feedFetchError", url)
            return
        feedinfo = podcast.feedinfo()
        feedinfo["latest_entries"] = [episode.get_data() for episode in episodes[:num_preview_episodes]]
        if hasattr(podcast, "alt_feeds"):
            feedinfo["altfeeds"] = podcast.alt_feeds
        else:
            logger.warning("No altfeeds attribute found in podcast")
            feedinfo["altfeeds"] = []
        pyotherside.send("feedinfo", feedinfo)

    def doexit(self):
        logger.info("closing db")
        Constants().close()

    async def get_podcast_preview(self, theurl, num_preview_episodes):
        await self.get_feedinfo(theurl, True, num_preview_episodes=num_preview_episodes)

    async def get_entries(self, searchoptions):
        """
        Get all podposts
        """
        logger.info("searching with: %s", searchoptions)
        triggered = False
        for offset, max, chunk in chunks(DictParser.iterator_from_searchoptions(searchoptions), 32):
            triggered = True
            pyotherside.send("episodeListData",chunk, offset, max)
        if not triggered:
            pyotherside.send("episodeListData", [], 0, 0)

    async def subscribe_podcast(self, url):
        """
        Subscribe a podcast
        """
        PodcastFactory().remove_podcast(url)
        Podcast.create_from_url(url)
        podcast_list = PodcastListFactory().get_podcast_list()
        podcast_list.add(url)
        pyotherside.send("subscribed", url)

    async def get_podcasts(self):
        """
        Get Podcast List
        """

        podcasts = []
        pclist = PodcastListFactory().get_podcast_list()

        for pc in pclist.get_podcasts_objects():
            podcasts.append(pc.feedinfo())

        pyotherside.send("podcastslist", podcasts)

    async def delete_podcast(self, url):
        """
        Delete a podcast from list
        """

        PodcastListFactory().get_podcast_list().delete_podcast(url)
        await self.get_podcasts()
        pyotherside.send("unsubscribed", url)

    async def move_archive(self, id):
        """
        move post to Archive
        """

        ArchiveFactory().get_archive().insert(id)
        pyotherside.send("movedToArchive")

    @async_threaded()
    async def refresh_podcast(self, url, moveto, download, limit=0, full_refresh=False):
        """
        Refresh one podcast
        """
        with lock_refresh:
            if not download:
                return

            podcast = PodcastFactory().get_podcast(url)

            for move, post in podcast.refresh(moveto, limit, full_refresh=full_refresh):
                page = movePost(move, post.id)
                pyotherside.send(
                    "updatesNotification", podcast.title, post.title, page
                )

            pyotherside.send("refreshFinished")

    @async_threaded()
    async def refresh_podcasts(self, moveto, download, limit=0, full_refresh=False):
        """
        Refresh all podcasts
        """
        with lock_refresh:
            if not download:
                return

            podcast_list = PodcastListFactory().get_podcast_list()

            for postid, podcasttitle, posttitle, move in podcast_list.refresh(moveto, limit,
                                                                               full_refresh=full_refresh):
                try:
                    pyotherside.send(
                        "updatesNotification", podcasttitle, posttitle, MovePostToPage(move).get_humanized_name()
                    )
                except Exception as e:
                    persistent_log.persist_log(LogType.Exception, msg="Could not move episode to according list",
                                               exception=e)
            pyotherside.send("refreshFinished")

    async def get_podcast_params(self, url):
        """
        Get Parameter set of podcast
        """

        podcast = PodcastFactory().get_podcast(url)
        pyotherside.send("podcastParams", podcast.get_params())

    async def set_podcast_params(self, url, params):
        """
        Put parameter set to podcast
        """

        podcast = PodcastFactory().get_podcast(url)
        podcast.set_params(params)

    async def render_html(self, data):
        """
        Render a temporary page
        """

        storepath = Constants().iconpath
        htmlfile = os.path.join(storepath, "page.html")
        with open(htmlfile, "wb") as h:
            h.write("<html><body>".encode())
            h.write(data.encode())
            h.write("</body></html".encode())
        pyotherside.send("htmlfile", htmlfile)

    async def nomedia(self, doset):
        """
        Set nomedia in config root
        """

        audiopath = os.path.join(Constants().audiofilepath, ".nomedia")
        iconpath = os.path.join(Constants().iconpath, ".nomedia")
        open(iconpath, "a").close()

        if doset == False:
            open(audiopath, "a").close()
        else:
            os.remove(audiopath)

    async def import_opml(self, opmlfile):
        """
        Import podcasts from opmlfile
        """

        pclist = PodcastListFactory().get_podcast_list()
        imported = pclist.import_opml(opmlfile)
        if imported > 0:
            pyotherside.send("opmlimported", imported)

    async def import_gpodder(self):
        """
        Import podcasts from opmlfile
        """

        pclist = PodcastListFactory().get_podcast_list()
        imported = pclist.import_gpodder()
        if imported > 0:
            pyotherside.send("opmlimported", imported)

    async def do_backup(self, with_store=True):
        """
        Create a backup tarball of store and icons
        """

        data_path = Constants().data_home
        backupdir = Constants().backuppath
        os.chdir(backupdir)
        filename = "podqast-%s.tar.gz" % time.strftime("%Y%m%d%H%M")
        tarfilename = os.path.join(backupdir, filename)

        try:
            tar = tarfile.open(tarfilename, "w:gz")
            storepath = "store"
            audiopath = "audio"

            def excludes(tf: tarfile.TarInfo):
                if tf.name == storepath and not with_store or tf.name == audiopath:
                    return None
                logger.debug("name: %s", tf.name)
                return tf

            tar.add(data_path, filter=excludes, arcname="")
            queuepath = "store/cadda373c547afec7792d24db35e484641d9637c8c0c18218eeadf1adee187f6.pickle"
            tar.add(os.path.join(data_path, queuepath), arcname=queuepath)
            tar.close()
        except:
            pyotherside.send("error", "Backup failed")
            return

        pyotherside.send("backupDone", tarfilename)

    async def write_opml(self):
        """
        Create opml file
        """

        podcasts = []
        pclist = PodcastListFactory().get_podcast_list()
        for pc in pclist.get_podcasts_objects():
            podcasts.append(
                {"name": pc.title, "xml_url": pc.url, "html_url": pc.link}
            )

        output_dir = Constants().backuppath
        os.chdir(output_dir)
        filename = "podqast-%s.opml" % time.strftime("%Y%m%d%H%M")
        opmlfilename = os.path.join(output_dir, filename)

        create_opml(opmlfilename, podcasts)


instance = AsyncWrapper(FeedParser())
