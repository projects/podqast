import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: migrationhandler

    signal migrationNeeded(bool is_needed)
    signal migrationDone
    signal migrationStart(int section, int miglen)
    signal migrationProgress(int section, int migpos)
    signal migrationSections(int param_start_section, int param_target_section)

    property int progress_max: 1
    property int progress_current: 1
    property int start_section: 0
    property int target_section: 1
    property int sections_todo: target_section - start_section
    property int current_section: 0
    property int section_ratio: 1 / sections_todo
    property int current_base_ratio: (sections_todo - (current_section - start_section))
                                     * section_ratio
    property double progress: current_base_ratio
                              + (section_ratio * (progress_current / progress_max))
    property bool running: false

    Component.onCompleted: {
        setHandler("migrationStart", migrationStart)
        setHandler("migrationProgress", migrationProgress)
        setHandler("migrationDone", migrationDone)
        setHandler("migrationSections", migrationSections)

        addImportPath(Qt.resolvedUrl('.'))
        importModule('MigrationHandler', function () {
            console.log("MigrationHandler is now imported")
            call('MigrationHandler.needs_migration', [], function (is_needed) {
                console.log("Migration is needed: " + is_needed)
                migrationNeeded(is_needed)
            })
        })
    }

    onMigrationSections: {
        start_section = param_start_section
        target_section = param_target_section
    }

    onMigrationStart: {
        progress_max = miglen
        current_section = section
        progress_current = 0
    }

    onMigrationProgress: {
        current_section = section
        progress_current = migpos
    }

    onMigrationDone: running = false

    function start_migration(strict) {
        call('MigrationHandler.run_migrations', [strict], function () {})
    }

    function migration_ended() {
        console.log("migration done")
        running = false
        migrationDone()
    }
}
