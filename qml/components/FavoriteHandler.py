# -*- coding: utf-8 -*-
import logging
from typing import Iterator

import pyotherside
import threading
import sys


sys.path.append("/usr/share/harbour-podqast/python")
from podcast.util import chunks
from podcast.podpost import PodpostFactory, Podpost
import podcast.favorite as favorite

logger = logging.getLogger(__name__)

def toggle_favorite(podpost, do_download=False):
    """
    Return a list of all archive posts
    """

    post = PodpostFactory().get_podpost(podpost)
    if not post:
        return

    pyotherside.send("favoriteToggled", podpost, post.toggle_fav(do_download))


def send_favorites(podurl=None):
    logger.debug("Sending favorites...")
    posts: Iterator[Podpost] = favorite.get_favorites(podurl)
    for offset, max, chunk in chunks(posts, 32):
        pyotherside.send("favoriteListData", offset, [post.get_data() for post in chunk])

class FavoriteHandler:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()
        self.bgthread2 = threading.Thread()
        self.bgthread2.start()

    def togglefavorite(self, podpost):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=toggle_favorite, args=[podpost]
        )
        self.bgthread.start()

    def getfavorites(self, podurl=None):
        if self.bgthread2.is_alive():
            return
        if podurl:
            self.bgthread2 = threading.Thread(
                target=send_favorites, args=[podurl]
            )
        else:
            self.bgthread2 = threading.Thread(target=send_favorites)
        self.bgthread2.start()


favoritehandler = FavoriteHandler()
