import QtQuick 2.0
import Sailfish.Silica 1.0
import "../../components/hints"

FirstTimeUseCounter {
    id: firstTimeUseCounter
    limit: 2
    defaultValue: 1
    property string hintComponent
    onActiveChanged: {
        if (value < limit) {
            console.info("Showing hint " + hintComponent)
            var comp = Qt.createComponent(hintComponent)
            if (comp.status === Component.Ready) {
                var obj = comp.createObject(parent)
            }
            firstTimeUseCounter.increase()
        }
    }
}
