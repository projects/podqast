import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    anchors.fill: slideNr < slides.length ? parent : null
    height: slideNr < slides.length ? parent.height : 0

    property var slides: []
    property int slideNr: -1
    property var slideconfig: null
    property bool slideloaded: true

    MouseArea {
        anchors.fill: parent

        onClicked: {
            console.info("clicked hint")
            touchInteractionHint.stop()
        }
    }

    Component.onCompleted: nextSlide()

    Loader {
        id: centeredComponent
        anchors.centerIn: parent
        width: Theme.iconSizeExtraLarge
        height: Theme.iconSizeExtraLarge
        property int index: slideNr
        source: !slideloaded ? "" : slideconfig.centeredComponent
        visible: !slideloaded ? false : slideconfig.centeredComponent !== undefined
        active: !slideloaded ? false : slideconfig.centeredComponent !== undefined
        onActiveChanged: console.info("loading component " + source)
    }

    InteractionHintLabel {
        anchors.bottom: parent.bottom
        width: parent.width
        visible: true

        opacity: touchInteractionHint.running ? 1.0 : 0.0
        Behavior on opacity {
            FadeAnimation {
                duration: 750
            }
        }

        text: !slideloaded ? "<error>" : slideconfig.text
    }
    Item {
        anchors.fill: parent
        visible: slideloaded && slideconfig.interactionHidden !== true
        onVisibleChanged: console.info(
                              "Slide " + slideNr + " hides the touchInteractionHint: " + !visible)
        TouchInteractionHint {
            id: touchInteractionHint

            direction: !slideloaded ? -1 : slideconfig.direction
            interactionMode: !slideloaded ? -1 : slideconfig.interactionMode
            anchors.verticalCenter: parent.bottom
            loops: !slideloaded ? 0 : slideconfig.loops
            onRunningChanged: if (!running && slideNr < slides.length) {
                                  console.info("running changed switched to next slide")
                                  nextSlide()
                              }
        }
    }

    function nextSlide() {
        slideNr++
        console.info("nextslide: " + slideNr)
        if (slideNr < slides.length) {
            slideconfig = slides[slideNr]
            slideloaded = true
            console.info("switching to hint slide " + (slideNr) + " : " + JSON.stringify(
                             slideconfig))
            touchInteractionHint.restart()
        } else {
            slideloaded = false
            slideconfig = null
            centeredComponent.visible = false
            touchInteractionHint.stop()
        }
    }
}
