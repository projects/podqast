"""
Factory test
"""

import sys

sys.path.append("../python")

from podcast.factory import Factory


def test_singleton():
    """
    Factory is a Singleton. Let's test, if it really is...
    """

    f1 = Factory()  # first instance: set storedir
    f2 = Factory()

    assert f1 == f2
