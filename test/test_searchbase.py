from podcast.search.Filter import *
from podcast.search.Order import *
from podcast.search.SearchBase import *
from test import setup_inbox_with_2_posts


def test_inbox_searchbase():
    entry1, entry2 = setup_inbox_with_2_posts()
    assert [entry2.id, entry1.id]  == [post.id for post in list(InboxSearchBase().iter_search(NoFilter(),NoOrder()))]
    assert [entry2.id, entry1.id]  == [post.id for post in list(InboxSearchBase().iter_search(PodcastFilter(entry1.podcast),NoOrder()))]
    assert []  == [post.id for post in list(InboxSearchBase().iter_search(PodcastFilter(None),NoOrder()))]
    assert [entry1.id, entry2.id]  == [post.id for post in list(InboxSearchBase().iter_search(NoFilter(),PodpostListInsertDateOrder(Direction.ASC)))]
    assert [entry1.id, entry2.id]  == [post.id for post in list(InboxSearchBase().iter_search(PodcastFilter(entry1.podcast),PodpostListInsertDateOrder(Direction.ASC)))]
    assert [entry2.id, entry1.id]  == [post.id for post in list(InboxSearchBase().iter_search(NoFilter(),PodpostListInsertDateOrder(Direction.DESC)))]
    assert [entry2.id, entry1.id]  == [post.id for post in list(InboxSearchBase().iter_search(NoFilter(),PodpostPublishedDateOrder(Direction.DESC)))]
    assert [entry1.id, entry2.id]  == [post.id for post in list(InboxSearchBase().iter_search(NoFilter(),PodpostPublishedDateOrder(Direction.ASC)))]

def test_title_filter():
    entry1, entry2 = setup_inbox_with_2_posts()
    assert [entry1.id]  == [post.id for post in list(AllSearchBase().iter_search(PodpostTextFilter("Milch"), NoOrder()))]
    assert [entry2.id]  == [post.id for post in list(AllSearchBase().iter_search(PodpostTextFilter("Alien"), NoOrder()))]
    assert [entry2.id]  == [post.id for post in list(AllSearchBase().iter_search(PodpostTextFilter("alien"), NoOrder()))]
    assert [entry2.id]  == [post.id for post in list(AllSearchBase().iter_search(PodpostTextFilter("Alien Wave"), NoOrder()))]
    assert []  == [post.id for post in list(AllSearchBase().iter_search(PodpostTextFilter("Alien not Wave"), NoOrder()))]
    assert []  == [post.id for post in list(AllSearchBase().iter_search(PodpostTextFilter("Wave Alien"), NoOrder()))]
    assert []  == [post.id for post in list(AllSearchBase().iter_search(PodpostTextFilter("Krautsalat"), NoOrder()))]
    assert [entry2.id]  == [post.id for post in list(AllSearchBase().iter_search(AndFilter(PodpostTextFilter("Alien"), PodpostTextFilter("Wave")), NoOrder()))]
    assert [entry1.id, entry2.id]  == [post.id for post in list(AllSearchBase().iter_search(OrFilter(PodpostTextFilter("Alien"), PodpostTextFilter("Milch")), PodpostPublishedDateOrder(Direction.ASC)))]
    assert []  == [post.id for post in list(AllSearchBase().iter_search(AndFilter(PodpostTextFilter("Alien"), PodpostTextFilter("Milch")), NoOrder()))]
