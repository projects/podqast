"""
Podpost tests
"""
import logging
import sys

import httpretty
from httpretty import HTTPretty

from test import xml_headers, read_testdata

sys.path.append("../python")

from podcast.podcast import Podcast
from podcast.podpost import Podpost, PodpostFactory

logger = logging.getLogger(__name__)


@httpretty.activate(allow_net_connect=False)
def test_podpost_save():
    """
    Test podpost saving
    """
    HTTPretty.register_uri(HTTPretty.GET, 'https://freakshow.fm/feed/opus/',
                           body=read_testdata('testdata/freakshow.rss'), adding_headers=xml_headers)

    p, episodes = Podcast.create_from_url('https://freakshow.fm/feed/opus/')
    e: Podpost = p.get_latest_entry()
    assert e.duration == 10 * 60 * 1000 + 55 * 1000
    e.position = e.duration - 59 * 1000
    id1 = e.id
    PodpostFactory().persist(e)
    n = PodpostFactory().get_podpost(e.id)
    id2 = n.id
    assert type(n) == Podpost
    assert id1 == id2
    assert n.duration == 10 * 60 * 1000 + 55 * 1000
    assert n.get_data()["listened"]


@httpretty.activate(allow_net_connect=False)
def test_podpost_save2():
    """
    Test podpost saving
    """
    feed_url = 'http://fakefeed.com/feed'
    HTTPretty.register_uri(HTTPretty.GET, feed_url,
                           body=read_testdata('testdata/fakefeed.xml'), adding_headers=xml_headers)

    p, episodes = Podcast.create_from_url(feed_url)
    assert len(episodes) == 1
    episode: Podpost = episodes[0]
    assert type(episode) == Podpost
    assert episode.htmlpart.endswith("consetetür</p>")
    assert episode.plainpart.strip().endswith("consetetür")
    assert episode.title.endswith("öäü")
    assert episode.get_data()['description'] == episode.plainpart
    assert episode.get_data()['detail'] == episode.htmlpart


def test_persist_bulk_empty():
    PodpostFactory().persist_bulk([])


def test_persist_bulk_nochapters():
    post = Podpost()
    post.guid = ""
    post.published = 0
    post.title = "testpost"
    PodpostFactory().persist_bulk([post])


def test_get_data_external_audio():
    post = Podpost()
    post.guid = ""
    post.published = 0
    post.title = "testpost"
    post.isaudio = True
    post.podcast = None
    post.get_data()


def test_tracker_removal():
    tests = [
        ("https://dts.podtrac.com/redirect.mp3/myfeed", "https://myfeed"),
        ("http://dts.podtrac.com/redirect.mp3/myfeed", "http://myfeed"),
        ("http://www.podtrac.com/pts/redirect.mp3/myfeed", "http://myfeed"),
        ("https://www.podtrac.com/pts/redirect.mp3/myfeed", "https://myfeed"),
        ("https://notracker.ru/feed/podtrac", "https://notracker.ru/feed/podtrac"),
    ]
    podpost = Podpost()
    for given, expected in tests:
        podpost.href = given

        assert podpost.remove_trackers() == expected
