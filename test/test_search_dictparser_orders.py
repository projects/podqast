from podcast.search.DictParser import *


def test_empty_options():
    assert create_filter_from_options({}) == None
    assert create_order_from_options({}) == PodpostPublishedDateOrder(Direction.DESC)


def test_order_only():
    assert create_order_from_options({"order_dir": "desc"}) == PodpostPublishedDateOrder(Direction.DESC)
    assert create_order_from_options({"order_dir": "asc"}) == PodpostPublishedDateOrder(Direction.ASC)


def test_order_published():
    assert create_order_from_options({"order": "published", "order_dir": "desc"}) == PodpostPublishedDateOrder(
        Direction.DESC)
    assert create_order_from_options({"order": "published", "order_dir": "asc"}) == PodpostPublishedDateOrder(
        Direction.ASC)


def test_order_inserted():
    assert create_order_from_options({"order": "inserted", "order_dir": "desc"}) == PodpostListInsertDateOrder(
        Direction.DESC)
    assert create_order_from_options({"order": "inserted", "order_dir": "asc"}) == PodpostListInsertDateOrder(
        Direction.ASC)


def test_real_case():
    options = {'fav_filter': False, 'order': 'published', 'order_dir': 'asc',
               'podcast_url': 'https://augenzu.podigee.io/feed/ogg', 'searchtext': ''}
    assert create_order_from_options(options) == PodpostPublishedDateOrder(Direction.ASC)
