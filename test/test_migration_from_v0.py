import os
import tempfile
from distutils.dir_util import copy_tree

from podcast import data_migration, favorite
from podcast.archive import ArchiveFactory
from podcast.constants import Constants, db
from podcast.external import ExternalFactory
from podcast.factory import Factory
from podcast.inbox import InboxFactory
from podcast.podcast import PodcastFactory
from podcast.podcastlist import PodcastListFactory
from podcast.podpost import PodpostFactory, Podpost, PodpostChapter
from podcast.queue import QueueFactory
from podcast.util import ilen
from test import conftest


def test_migration_v1():
    oldhome = os.environ["PODQAST_HOME"]
    try:
        with tempfile.TemporaryDirectory() as tmpdir:
            copy_tree(os.path.join(os.path.dirname(__file__), "testdata/migrationtests_v1/"), tmpdir)
            resetSingletons(tmpdir)
            data_migration.run_migrations(False)
            assert os.path.exists(os.path.join(tmpdir, "dataversion"))
            assert Factory().get_store().count() == 1694
            podcasts = list(PodcastListFactory().get_podcast_list().get_podcasts())
            assert len(podcasts) == 25
            for podcast in PodcastListFactory().get_podcast_list().get_podcasts_objects():
                assert podcast != None
                assert podcast.count_episodes() > 0
                assert podcast.title
            hrefs = list(tup[0] for tup in Podpost.select(Podpost.href).tuples())
            assert len(hrefs) > 1000
            assert len(hrefs) == len(set(hrefs))
            assert Podpost.select().count() == 2411
            assert len(ArchiveFactory().get_archive().get_podposts()) == 118
            assert ilen(favorite.get_favorites()) == 6
            assert ilen(QueueFactory().get_queue().get_podposts()) == 6
            assert ilen(ExternalFactory().get_external().get_podposts()) == 0
            assert ilen(InboxFactory().get_inbox().get_podposts()) == 42
            assert PodpostChapter.select().count() == 9293
            assert (130 * 60 + 54) * 1000 == QueueFactory().get_queue()._get_top().duration

            PodcastFactory().remove_podcast("http://minkorrekt.de/feed/opus/")
            assert ilen(PodcastListFactory().get_podcast_list().get_podcasts()) == 24
            assert Podpost.select().count() == 2223
    finally:
        db.close()
        reset(oldhome)


def reset(oldhome):
    resetSingletons(oldhome)


def resetSingletons(storagepath):
    Factory._instances = {}
    PodcastListFactory._instances = {}
    PodcastFactory._instances = {}
    PodpostFactory._instances = {}
    QueueFactory._instances = {}
    ArchiveFactory._instances = {}
    ExternalFactory._instances = {}
    InboxFactory._instances = {}
    conftest.init_db_and_storage_from_temp(storagepath)

