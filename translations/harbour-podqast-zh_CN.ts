<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="9"/>
        <source>../data/about.txt</source>
        <translation>关于</translation>
    </message>
</context>
<context>
    <name>Archive</name>
    <message>
        <location filename="../qml/pages/Archive.qml" line="45"/>
        <source>Manage Podcasts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="53"/>
        <source>History</source>
        <translation>历史</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="61"/>
        <source>Favorites</source>
        <translation>收藏</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="70"/>
        <source>External Audio</source>
        <translation>外部音频</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="83"/>
        <source>Rendering</source>
        <translation>生成</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="84"/>
        <source>Collecting Podcasts</source>
        <translation>收藏的播客</translation>
    </message>
</context>
<context>
    <name>BackupButtons</name>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="17"/>
        <source>Backup done</source>
        <translation>备份完成</translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="18"/>
        <location filename="../qml/components/BackupButtons.qml" line="19"/>
        <source>Backup done to </source>
        <translation>备份完成到</translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="26"/>
        <source>OPML file saved</source>
        <translation>已保存 OPML 文件</translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="27"/>
        <location filename="../qml/components/BackupButtons.qml" line="28"/>
        <source>OPML file saved to </source>
        <translation>OPML 文件保存到</translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="36"/>
        <source>Backup</source>
        <translation>备份</translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="52"/>
        <source>Save to OPML</source>
        <translation>保存到 OPML</translation>
    </message>
</context>
<context>
    <name>Chapters</name>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="49"/>
        <source>Chapters</source>
        <translation>章节</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="108"/>
        <source>No chapters</source>
        <translation>无章节</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="109"/>
        <source>Rendering chapters</source>
        <translation>更新章节</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="37"/>
        <source> chapters</source>
        <translation>章节</translation>
    </message>
</context>
<context>
    <name>DataMigration</name>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="16"/>
        <source>Update done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="17"/>
        <source>Successfully migrated data to the current version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="41"/>
        <source>Data Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="73"/>
        <source>Because the data format changed, podqast will need to migrate your data to the new format. You can use one of the buttons below to create a backup of your data for recovery.</source>
        <extracomment>general text explaining what the migration does</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="80"/>
        <source>&lt;b&gt;Whats New?&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Some of you lost episodes in the last migration. Because we found this pretty sad, we&apos;re trying to recover them now.&lt;/li&gt;&lt;li&gt;If you are missing old episodes of feeds in general and this didn&apos;t help, try the force refresh button in the settings (We added support for Pagination).&lt;/li&gt;&lt;li&gt;Podcasts are now sorted alphabetically, we hope you like that!&lt;/li&gt;&lt;li&gt;Gpodder should work again.&lt;/li&gt;&lt;li&gt;Check the new Log Page next to the settings.&lt;/li&gt;&lt;li&gt;If you&apos;re streaming an episode and the download finishes, we&apos;re now switching to the downloaded file automatically.&lt;/li&gt;&lt;li&gt;Also more speed, bugfixes and code cleanups.&lt;/li&gt;&lt;/ul&gt;&lt;br&gt;If you want to contribute to podQast, you can help translating the app or report issues on GitLab.</source>
        <extracomment>whatsnew section of the migration</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="87"/>
        <source>The migration is running, please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="99"/>
        <source>Start Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="125"/>
        <source>Try to ignore errors during migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="137"/>
        <source>We couldnt migrate your data completly. You can restart the app and try it in the non-strict mode. Please consider reporting this as a bug in the Issue-Tracker.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="143"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="148"/>
        <source>Open issue tracker</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Discover</name>
    <message>
        <location filename="../qml/pages/Discover.qml" line="13"/>
        <source>Discover</source>
        <translation type="unfinished">发现</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="26"/>
        <source>Search on gpodder.net</source>
        <translation>在gpodder.net搜索</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="30"/>
        <source>Search on fyyd.de</source>
        <translation>在fyyd.de搜索</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="34"/>
        <source>Tags...</source>
        <translation>标签……</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="38"/>
        <source>Url...</source>
        <translation>链接……</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="42"/>
        <source>Import...</source>
        <translation>导入……</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="46"/>
        <source>Export...</source>
        <translation>导出……</translation>
    </message>
</context>
<context>
    <name>DiscoverExport</name>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="24"/>
        <source>Export</source>
        <translation>导出</translation>
    </message>
</context>
<context>
    <name>DiscoverImport</name>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="23"/>
        <source> Podcasts imported</source>
        <translation>已导出播客</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="25"/>
        <location filename="../qml/pages/DiscoverImport.qml" line="27"/>
        <source> Podcasts imported from OPML</source>
        <translation>播客导出自 OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="46"/>
        <source>Discover by Importing</source>
        <translation>导出发现</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="59"/>
        <source>Pick an OPML file</source>
        <translation>选择 OPML 文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="60"/>
        <source>OPML file</source>
        <translation>OPML 文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="80"/>
        <source>Import OPML</source>
        <translation>导出 OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="82"/>
        <source>Import OPML File</source>
        <translation>导出 OPML 文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="102"/>
        <source>Import from Gpodder</source>
        <translation>从 Gpodder 导入</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="104"/>
        <source>Import Gpodder Database</source>
        <translation>导入 Gpodder 数据库</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="128"/>
        <source>Please note: Importing does take some time!</source>
        <translation>请注意：导入将会需要一些时间!</translation>
    </message>
</context>
<context>
    <name>DiscoverTags</name>
    <message>
        <location filename="../qml/pages/DiscoverTags.qml" line="42"/>
        <source>Discover Tags</source>
        <translation>找到标签</translation>
    </message>
</context>
<context>
    <name>DiscoverUrl</name>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="26"/>
        <source>Discover by URL</source>
        <translation>通过链接找到</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="46"/>
        <source>Enter podcasts&apos; feed url</source>
        <translation>输入播客及订阅链接</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="64"/>
        <source>Please enter feed urls here - not web page urls</source>
        <translation>请在此输入订阅链接而非网页链接</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="77"/>
        <source>Discover</source>
        <translation>发现</translation>
    </message>
</context>
<context>
    <name>EpisodeContextMenu</name>
    <message>
        <location filename="../qml/components/EpisodeContextMenu.qml" line="96"/>
        <source>Select chapters</source>
        <translation type="unfinished">选择章节</translation>
    </message>
</context>
<context>
    <name>External</name>
    <message>
        <location filename="../qml/pages/External.qml" line="49"/>
        <source>External Audio</source>
        <translation>外部音频</translation>
    </message>
    <message>
        <location filename="../qml/pages/External.qml" line="66"/>
        <source>Rendering</source>
        <translation>生成</translation>
    </message>
    <message>
        <location filename="../qml/pages/External.qml" line="67"/>
        <source>Collecting Posts</source>
        <translation>收藏内容</translation>
    </message>
</context>
<context>
    <name>Favorites</name>
    <message>
        <location filename="../qml/pages/Favorites.qml" line="46"/>
        <source>Favorites</source>
        <translation>收藏</translation>
    </message>
</context>
<context>
    <name>FeedParserPython</name>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="149"/>
        <source>Auto-Post-Limit reached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="150"/>
        <source>for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="151"/>
        <source>Auto-Post-Limit reached for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="159"/>
        <source>Error</source>
        <translation type="unfinished">错误</translation>
    </message>
</context>
<context>
    <name>History</name>
    <message>
        <location filename="../qml/pages/History.qml" line="45"/>
        <source>History</source>
        <translation>历史</translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="62"/>
        <source>Rendering</source>
        <translation>生成</translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="63"/>
        <source>Collecting Posts</source>
        <translation>收藏内容</translation>
    </message>
</context>
<context>
    <name>Inbox</name>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="50"/>
        <source>Moving all posts to archive</source>
        <translation>移动所有内容到存档</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="78"/>
        <source>Clear Inbox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="94"/>
        <source>No new posts</source>
        <translation>无新内容</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="95"/>
        <source>Pull down to Discover new podcasts, get posts from Library, or play the Playlist</source>
        <translation>下拉以找到新播客、从库获取内容或播放播放列表中的内容</translation>
    </message>
</context>
<context>
    <name>Log</name>
    <message>
        <location filename="../qml/pages/Log.qml" line="25"/>
        <source>Logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="63"/>
        <source>No logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="74"/>
        <source>Skipped refresh on &apos;%1&apos;, because the server responded with &apos;Not Modified&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="77"/>
        <source>Updated &apos;%1&apos;. %2 new episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="80"/>
        <source>&apos;%1&apos; could not be refreshed because: %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainCarousel</name>
    <message>
        <location filename="../qml/pages/MainCarousel.qml" line="33"/>
        <source>Inbox</source>
        <translation type="unfinished">收件箱</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainCarousel.qml" line="33"/>
        <source>Playlist</source>
        <translation type="unfinished">播放列表</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainCarousel.qml" line="33"/>
        <source>Library</source>
        <translation type="unfinished">库</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainCarousel.qml" line="66"/>
        <source>Settings</source>
        <translation type="unfinished">设置</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainCarousel.qml" line="71"/>
        <source>Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainCarousel.qml" line="75"/>
        <source>About</source>
        <translation type="unfinished">关于</translation>
    </message>
</context>
<context>
    <name>MainPageUsageHint</name>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="63"/>
        <source>You can see whats going on by looking into the log down here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="68"/>
        <source>Podqast supports pagination of feeds. If you are missing old episodes of feeds, you can find a force refresh button in the settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="21"/>
        <source>On an episode image, this icon shows that it is downloaded...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="9"/>
        <source>Hi there, I gonna give you a quick tour to show you basic and new features.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="15"/>
        <source>Quick touch on an episode to see its description, long press it to add it to queue or play it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="28"/>
        <source>...currently playing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="35"/>
        <source>...already listened.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="42"/>
        <source>In the dock below, this icon means the file currently played is streaming from the internet right now...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="49"/>
        <source>...the sleeptimer is active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="56"/>
        <source>...a playrate different from 1 is set.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Player</name>
    <message>
        <location filename="../qml/pages/Player.qml" line="16"/>
        <source>Auto-play next episode in queue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="16"/>
        <source>Stop after each episode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="53"/>
        <source>Audio playrate</source>
        <translation>音频播放率</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="90"/>
        <source>Sleep timer</source>
        <translation>睡眠计时器</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="90"/>
        <source> running...</source>
        <translation>运行中……</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="213"/>
        <source> chapters</source>
        <translation>章节</translation>
    </message>
</context>
<context>
    <name>PlayerHint</name>
    <message>
        <location filename="../qml/components/hints/PlayerHint.qml" line="8"/>
        <source>Select if the next item of the playlist should be played automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/PlayerHint.qml" line="13"/>
        <source>Click the image to adjust the playrate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PodcastDirectorySearchPage</name>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="27"/>
        <source>Discover by Search</source>
        <translation type="unfinished">通过搜索找到</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="32"/>
        <source>Search</source>
        <translation type="unfinished">搜索</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="56"/>
        <source>Loading results for &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="60"/>
        <source>No results</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PodcastItem</name>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="24"/>
        <source>Refresh</source>
        <translation>刷新&gt;</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="32"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="44"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="50"/>
        <source>Deleting Podcast</source>
        <translation>正在删除播客</translation>
    </message>
</context>
<context>
    <name>PodcastSettings</name>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="11"/>
        <source>Settings</source>
        <translation type="unfinished">设置</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="45"/>
        <source>URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="57"/>
        <source>Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="39"/>
        <source>%1 Posts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="83"/>
        <source>Move new post to</source>
        <translation>移动新内容到</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="86"/>
        <source>Inbox</source>
        <translation>收件箱</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="89"/>
        <source>Top of Playlist</source>
        <translation>播放列表顶部</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="92"/>
        <source>Bottom of Playlist</source>
        <translation>播放列表底部</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="95"/>
        <source>Archive</source>
        <translation>存档</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="102"/>
        <source>Automatic post limit</source>
        <translation>自动限制内容</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="119"/>
        <source>Audio playrate</source>
        <translation>音频播放率</translation>
    </message>
</context>
<context>
    <name>Podcastsearch</name>
    <message>
        <location filename="../qml/pages/Podcastsearch.qml" line="30"/>
        <source>Search by Tag...</source>
        <translation>按标签搜索……</translation>
    </message>
</context>
<context>
    <name>PodpostList</name>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="107"/>
        <source>Rendering</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="108"/>
        <source>Creating items</source>
        <translation>创建新项目</translation>
    </message>
</context>
<context>
    <name>PostDescription</name>
    <message>
        <location filename="../qml/pages/PostDescription.qml" line="27"/>
        <source>Open in browser</source>
        <translation>用浏览器打开</translation>
    </message>
</context>
<context>
    <name>PostListItem</name>
    <message>
        <location filename="../qml/components/PostListItem.qml" line="105"/>
        <source>playing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PostListItem.qml" line="109"/>
        <source>listened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PostListItem.qml" line="113"/>
        <source>remaining</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrefAboutMenu</name>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="8"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="12"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="17"/>
        <source>Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="22"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
</context>
<context>
    <name>Queue</name>
    <message>
        <location filename="../qml/pages/Queue.qml" line="54"/>
        <source>No posts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="55"/>
        <source>Pull down to Discover new podcasts or get posts from Inbox or Library</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RefreshPodcastsPulley</name>
    <message>
        <location filename="../qml/components/RefreshPodcastsPulley.qml" line="11"/>
        <source>refresh podcasts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/RefreshPodcastsPulley.qml" line="24"/>
        <source>refreshing: </source>
        <translation type="unfinished">刷新：</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="32"/>
        <source>Global Podcast Settings</source>
        <translation>全局播客设置</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="45"/>
        <source>When touching episode image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="51"/>
        <source>Show Podcast Episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="73"/>
        <source>Move new post to</source>
        <translation>移动新内容到</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="76"/>
        <location filename="../qml/pages/Settings.qml" line="231"/>
        <source>Inbox</source>
        <translation>收件箱</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="85"/>
        <location filename="../qml/pages/Settings.qml" line="240"/>
        <source>Archive</source>
        <translation>存档</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="97"/>
        <source>Automatic post limit</source>
        <translation>自动限制内容</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="123"/>
        <source>Refresh time</source>
        <translation>刷新时间</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="140"/>
        <source>Audio playrate</source>
        <translation>音频播放比率</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="127"/>
        <source>off</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="156"/>
        <source>Time left to ignore for listened-mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="251"/>
        <source>Download/Streaming</source>
        <translation>下载流媒体</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="342"/>
        <source>Development</source>
        <translation>开发</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="353"/>
        <source>Experimental features</source>
        <translation>实验功能</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="79"/>
        <location filename="../qml/pages/Settings.qml" line="234"/>
        <source>Top of Playlist</source>
        <translation>播放列表顶部</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="48"/>
        <source>Play Episode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="54"/>
        <source>Show Episode Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="57"/>
        <source>Add To Queue Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="60"/>
        <source>Add To Queue Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="82"/>
        <location filename="../qml/pages/Settings.qml" line="237"/>
        <source>Bottom of Playlist</source>
        <translation>播放列表底部</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="101"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="160"/>
        <source>s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="172"/>
        <source>Sleep timer</source>
        <translation>睡眠计时器</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="176"/>
        <source>min</source>
        <translation>分钟</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="195"/>
        <source>External Audio</source>
        <translation>外部音频</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="207"/>
        <source>Allow external audio</source>
        <translation>允许外部音频</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="215"/>
        <source>Will take data from
</source>
        <translation>将会获取数据自</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="228"/>
        <source>Move external audio to</source>
        <translation>移动外部音频到</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="268"/>
        <source>Download Playlist Posts</source>
        <translation>下载播放列表内容</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="277"/>
        <source>Download on Mobile</source>
        <translation>通过移动网络下载</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="286"/>
        <source>Keep Favorites downloaded</source>
        <translation>保留下载的收藏</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="295"/>
        <source>Will save data in
</source>
        <translation>将会保存数据到</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="323"/>
        <source>System</source>
        <translation>系统</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="334"/>
        <source>Audio viewable in system</source>
        <translation>音频在系统可见</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="186"/>
        <source>Auto-play next episode in queue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="362"/>
        <source>Force refresh of old episodes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SubscribePodcast</name>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="78"/>
        <source>Could not load feed %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="167"/>
        <source>Configure</source>
        <translation type="unfinished">配置</translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="167"/>
        <source>Subscribe</source>
        <translation type="unfinished">订阅</translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="208"/>
        <source>Feed alternatives</source>
        <translation type="unfinished">可选订阅溜</translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="231"/>
        <source>Latest Post</source>
        <translation type="unfinished">最新的内容</translation>
    </message>
</context>
<context>
    <name>Wizzard1</name>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="30"/>
        <source>../data/about.txt</source>
        <translation>向导1</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="48"/>
        <source>Next</source>
        <translation>下一个</translation>
    </message>
</context>
<context>
    <name>Wizzard2</name>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="36"/>
        <source>Let&apos;s start...</source>
        <translation>向导2</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="37"/>
        <source>Back to info</source>
        <translation>返回到信息</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="54"/>
        <source> Podcasts imported</source>
        <translation> 已导入播客</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="55"/>
        <location filename="../qml/pages/Wizzard2.qml" line="56"/>
        <source> Podcasts imported from OPML</source>
        <translation>已从 OPML 输入播客</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="73"/>
        <source>You are now able to import old stuff. You can import from Discover lateron. Please note: Importing does take some time!</source>
        <translation>你现在可以输入旧的内容。你可以稍后从发现输入。请注意：导入需要一些时间!</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="102"/>
        <source>Pick an OPML file</source>
        <translation>选择 OPML 文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="103"/>
        <source>OPML file</source>
        <translation>OPML 文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="125"/>
        <source>Import OPML</source>
        <translation>导入 OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="127"/>
        <source>Import OPML File</source>
        <translation>输入 OPML 文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="146"/>
        <source>Import from Gpodder</source>
        <translation>从 Gpodder 导入</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="149"/>
        <source>Import Gpodder Database</source>
        <translation>导入 Gpodder 数据库</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="175"/>
        <source>When you now start you can discover new podcasts. After that you can for example select new posts to play from the Library.</source>
        <translation>你现在开始可以寻找新的播客。之后，你可以从库中选择要播放的新内容。</translation>
    </message>
</context>
<context>
    <name>harbour-podqast</name>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="356"/>
        <source>New posts available</source>
        <translation>有可获取的新内容</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="357"/>
        <source>Click to view updates</source>
        <translation>点击以查看更新</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="358"/>
        <source>New Posts are available. Click to view.</source>
        <translation>有可用新内容，点击以查看。</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="375"/>
        <location filename="../qml/harbour-podqast.qml" line="376"/>
        <source>PodQast message</source>
        <translation>PodQast 消息</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="377"/>
        <source>New PodQast message</source>
        <translation>新 PodQast 消息</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="397"/>
        <source> Podcasts imported</source>
        <translation> 播客已导入</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="399"/>
        <location filename="../qml/harbour-podqast.qml" line="401"/>
        <source> Podcasts imported from OPML</source>
        <translation>已从 OPML 导入播客</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="463"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="464"/>
        <location filename="../qml/harbour-podqast.qml" line="465"/>
        <source>Audio File not existing</source>
        <translation>音频文件不存在</translation>
    </message>
</context>
</TS>
