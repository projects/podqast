import datetime
import json
import logging
from enum import Enum
from typing import Iterator

from peewee import AutoField, CharField, TextField, DateTimeField
from podcast.constants import BaseModel

logger = logging.getLogger(__name__)
LOG_LIMIT = 1000


class LogType(Enum):
    AutoPostLimit = "AutoPostLimit",
    Exception = "Exception",
    NetworkError = "NetworkError",
    FeedParseError = "FeedParseError"
    FeedEpisodeParseError = "FeedEpisodeParseError"
    AddPodcastError = "AddPodcastError"
    SuccessfulRefresh = "SuccessfulRefresh"
    Refresh304 = "Refresh304"
    FeedRedirect = "FeedRedirect"
    UnknownPodpostId = "UnknownPodpostId"


class LogMessage(BaseModel):
    id = AutoField()
    messagetype = CharField(null=False)
    params = TextField()
    insert_date = DateTimeField(default=datetime.datetime.now)

    def to_dict(self):
        return {
            "messagetype": str(self.messagetype),
            "params": str(self.params),
            "insert_date": self.insert_date
        }


def convert_params(params):
    converted = {}
    for k, v in params.items():
        if isinstance(v, Exception) and hasattr(v, "message"):
            converted[k] = v.message
        else:
            converted[k] = str(v)
    return converted


def persist_log(type: LogType, **params):
    msg = LogMessage()
    msg.messagetype = type.value
    msg.params = json.dumps(convert_params(params))
    logger.info("Saving log to peristent logs: %s %s", msg.messagetype, msg.params)
    msg.save()
    over_limit = LogMessage.select().count() - LOG_LIMIT
    if over_limit > 0:
        for msg_id in LogMessage.select(LogMessage.id).order_by(LogMessage.insert_date.asc()).limit(
                over_limit).tuples():
            LogMessage.delete_by_id(msg_id[0])


def get_log_messages() -> Iterator[LogMessage]:
    return LogMessage.select().order_by(LogMessage.insert_date.desc()).iterator()
