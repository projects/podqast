"""
External Audio data list
"""

import sys
import time
from typing import Iterator, Tuple

from peewee import CharField
from podcast import POST_ID_TYPE
from podcast.AbstractPodpostListEntry import AbstractPodpostListEntry
from podcast.podpost import PodpostFactory, Podpost

sys.path.append("../")

from podcast.singleton import Singleton

externalname = "the_external"


class ExternalEntry(AbstractPodpostListEntry):
    filename = CharField(unique=True)

    @classmethod
    def of_podpost(cls, postid: POST_ID_TYPE, filename: str):
        if type(postid) == str:
            postid = POST_ID_TYPE(postid)
        if type(postid) != POST_ID_TYPE:
            raise ValueError("Postid must be valid")
        if PodpostFactory().get_podpost(postid) == None:
            raise ValueError("Trying to add post %s to %s, but post does not exist" % (str(cls), postid))
        entry = cls()
        entry.podpost = postid
        entry.insert_date = time.time()
        entry.filename = filename
        return entry


class External:
    """
    The  External audio list. It has a list of podposts
    """

    def save(self):
        """
        pickle this element
        """
        raise NotImplementedError()

    def insert(self, podpost: POST_ID_TYPE, filename):
        """
        Insert an podost
        """
        ExternalEntry.of_podpost(podpost, filename).save(force_insert=True)

    def get_podposts(self) -> Iterator[POST_ID_TYPE]:
        """
        get the list of podposts
        """
        for entry in ExternalEntry.select().order_by(ExternalEntry.insert_date.desc()).iterator():
            yield entry.podpost_id

    def get_podposts_objects(self) -> Iterator[Tuple[str, Podpost]]:
        for entry in ExternalEntry.select().order_by(ExternalEntry.insert_date.desc()).iterator():
            yield entry.filename, entry.podpost

    def remove_podpost(self, podpost: POST_ID_TYPE):
        """
        Remove a podpost from archive
        """
        ExternalEntry.delete_by_id(podpost)

    def remove_by_filename(self, filename: str):
        ExternalEntry.delete().where(ExternalEntry.filename == filename).execute()

    def has_file(self, filename: str):
        return ExternalEntry.select().where(ExternalEntry.filename == filename).exists()


class ExternalFactory(metaclass=Singleton):
    """
    Factory  which creates an Archive if it does not exist or gets the pickle
    otherwise if returns the Singleton
    """

    def get_external(self) -> External:
        """
        Get the Archive
        """

        return External()
