import logging
import time
from typing import List

from peewee import DateTimeField, ForeignKeyField
from podcast import POST_ID_TYPE
from podcast.constants import BaseModel
from podcast.podpost import Podpost, PodpostFactory
from podcast.util import chunks

logger = logging.getLogger(__name__)


class AbstractPodpostListEntry(BaseModel):
    insert_date: DateTimeField = DateTimeField(default=time.time())  # unused yet
    podpost: ForeignKeyField = ForeignKeyField(Podpost, primary_key=True, on_delete='CASCADE')

    @classmethod
    def insert_many_podposts(cls, post_ids: List[POST_ID_TYPE]):
        for _, _, chunk in chunks(post_ids, 500):
            cls.insert_many(({'podpost': id} for id in chunk),
                            fields=[cls.podpost]).on_conflict_ignore().execute()

    @classmethod
    def of_podpost(cls, postid: POST_ID_TYPE):
        if type(postid) != POST_ID_TYPE:
            postid = POST_ID_TYPE(postid)
        if not PodpostFactory().exists(postid):
            # db not started?
            # make sure db is properly closed
            logger.error("Trying to add post %s to %s, but post does not exist" % (str(cls), postid))
        entry = cls()
        entry.podpost = postid
        entry.insert_date = time.time()
        return entry
