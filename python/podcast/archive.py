"""
Archive of listened poposts
"""
import logging
import sys
import time
from typing import List, Iterator

from peewee import ModelSelect, IntegrityError
from podcast import POST_ID_TYPE
from podcast.AbstractPodpostListEntry import AbstractPodpostListEntry
from podcast.podcast import PodcastFactory

sys.path.append("../")

from podcast.singleton import Singleton
from podcast.podpost import Podpost

archivename = "the_archive"

logger = logging.getLogger(__name__)


class ArchiveEntry(AbstractPodpostListEntry):
    pass


class Archive:
    """
    The podcast Archive. It has a list of podposts
    """

    def save(self):
        """
        todo: remove this method
        """

        raise NotImplementedError()

    def insert(self, podpost: POST_ID_TYPE, overwrite_insert_date=True):
        """
        Insert an podost
        """
        try:
            ArchiveEntry.of_podpost(podpost).save(force_insert=True)
        except IntegrityError:
            if overwrite_insert_date:
                entry: ArchiveEntry = ArchiveEntry.get_by_id(podpost)
                entry.insert_date = time.time()
                entry.save()

    def bulk_insert(self, podposts: List[POST_ID_TYPE]):
        ArchiveEntry.insert_many_podposts(podposts)

    def get_podposts(self, sorted_by_date=False) -> List[POST_ID_TYPE]:
        """
        get the list of podposts
        """
        query: ModelSelect = ArchiveEntry.select()
        if sorted_by_date:
            query = query.order_by(ArchiveEntry.insert_date.desc())
        return list(e.podpost_id for e in query)

    def get_podpost_objects(self, url_filter=None, filter_favorite=False, sort_by_insertdate=False, limit=0) -> \
            Iterator[Podpost]:
        query: ModelSelect = ArchiveEntry.select(Podpost).join(Podpost)
        if url_filter:
            query = query.where(Podpost.podcast == PodcastFactory().get_podcast(url_filter))
        if filter_favorite:
            query = query.where(Podpost.favorite == True)
        if sort_by_insertdate:
            query = query.order_by(Podpost.insert_date.desc())
        if limit != 0:
            query = query.limit(limit)
        for entry in query.iterator():
            yield entry.podpost

    def remove_podpost(self, podpost: POST_ID_TYPE):
        """
        Remove a podpost from archive
        """

        ArchiveEntry.delete_by_id(podpost)


class ArchiveFactory(metaclass=Singleton):
    """
    Factory  which creates an Archive if it does not exist or gets the pickle
    otherwise if returns the Singleton
    """

    def get_archive(self) -> Archive:
        """
        Get the Archive
        """

        return Archive()
