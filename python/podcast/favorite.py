from podcast.podcast import PodcastFactory
from podcast.podpost import Podpost

def get_favorites(podurl=None):
    query = Podpost.select()
    if podurl:
        podid = PodcastFactory().get_podcast(podurl).id
        query = query.where(Podpost.favorite and podid == Podpost.podcast)
    else:
        query = query.where(Podpost.favorite)
    return query.order_by(Podpost.insert_date.desc()).iterator()
