from abc import abstractmethod, ABC
from typing import Dict

from peewee import ModelSelect, Expression
from podcast.podcast import Podcast
from podcast.podpost import Podpost


class Filter(ABC):
    def apply_filter(self, query: ModelSelect, search_env: Dict) -> ModelSelect:
        return query.where(self.get_where_expression(search_env))

    @abstractmethod
    def get_where_expression(self, search_env: Dict) -> Expression:
        raise ValueError("Do not call abstract method")


    def __eq__(self, other):
        if other is None:
            return False
        if other.__class__ == self.__class__:
            return self.__dict__ == other.__dict__
        else:
            return False


class NoFilter(Filter):
    """
    Warning: do not use as part of other filters
    """

    def apply_filter(self, query: ModelSelect, _) -> ModelSelect:
        return query

    def get_where_expression(self, search_env: Dict) -> Expression:
        raise NotImplementedError()


class PodcastFilter(Filter):
    def __init__(self, podcast: Podcast) -> None:
        super().__init__()
        self.podcast = podcast

    def get_where_expression(self, search_env: Dict) -> Expression:
        return Podpost.podcast == self.podcast


class PodpostTextFilter(Filter):

    def __init__(self, searchstring: str) -> None:
        super().__init__()
        self.searchstring = searchstring

    def get_where_expression(self, search_env: Dict) -> Expression:
        return Podpost.title.contains(self.searchstring) or Podpost.plainpart.contains(self.searchstring)


class PodpostListenedFilter(Filter):
    def __init__(self, filterval) -> None:
        super().__init__()
        self.filterval = filterval

    def get_where_expression(self, search_env: Dict) -> Expression:
        return Podpost.listened == self.filterval


class FavoriteFilter(Filter):
    def __init__(self,filterval) -> None:
        super().__init__()
        self.filterval = filterval

    def get_where_expression(self, search_env: Dict) -> Expression:
        return Podpost.favorite == self.filterval


class BiFilter(Filter):
    def __init__(self, left: Filter, right: Filter) -> None:
        super().__init__()
        self.left = left
        self.right = right

    @abstractmethod
    def get_where_expression(self, search_env: Dict) -> Expression:
        raise ValueError("Do not call abstract method")


class AndFilter(BiFilter):
    def get_where_expression(self, search_env: Dict) -> Expression:
        return self.left.get_where_expression(search_env) & self.right.get_where_expression(search_env)


class OrFilter(BiFilter):
    def get_where_expression(self, search_env: Dict) -> Expression:
        return self.left.get_where_expression(search_env) | self.right.get_where_expression(search_env)
