import datetime
import hashlib
import logging
import os
import time
from typing import List, Tuple, Dict, Type

import pyotherside

from peewee import Model
from playhouse.migrate import migrate
from podcast import POST_ID_TYPE, podcastlist
from podcast.archive import ArchiveEntry, archivename
from podcast.constants import Constants, db
from podcast.external import externalname, ExternalEntry, ExternalFactory
from podcast.factory import Factory
from podcast.inbox import InboxEntry, inboxname, Inbox
from podcast.persistent_log import LogMessage
from podcast.podcast import PodcastFactory, Podcast
from podcast.podcastlist import listname
from podcast.podpost import Podpost, PodpostFactory, PodpostChapter
from podcast.queue import QueueFactory, QueueData
from podcast.util import chunks

logger = logging.getLogger(__name__)

target_data_version = 6


def needs_migration():
    def app_is_pristine():
        if get_versionnumber() != 0:
            logger.debug("app version %s", get_versionnumber())
            return False
        if os.path.exists(Constants().storepath):
            i = len(os.listdir(Constants().storepath))
            logger.debug("store exists with %d items", i)
            return i == 0

        return True

    if get_versionnumber() >= target_data_version:
        logger.info("Dataversion is fine (%d,%d).", get_versionnumber(), target_data_version)
        return False
    else:
        if app_is_pristine():
            logger.info("App is pristine, initializing db")
            setup_db()
            set_versionnumber(target_data_version)
            return False
        else:
            logger.info("App is not pristine, but has old data version. Stating migration from %d to %d",
                        get_versionnumber(), target_data_version)
            return True


def run_migrations(strict=True):
    current_version = get_versionnumber()
    start_version = current_version
    logger.info("Base migration version is %s", current_version)
    pyotherside.send("migrationSections", start_version, target_data_version)
    if (current_version < 1):
        logger.info("migration1: migrating pickles to sqlite")
        setup_db()
        logger.info("Migrating from version %d to version 1", current_version)
        podcasts = get_podcastlist_from_pickles()
        pyotherside.send("migrationStart", 1, len(podcasts))
        i: int = 0
        post_hash_to_id: Dict[str:POST_ID_TYPE] = {}
        for podcast_url in podcasts:
            podcast_dict = Factory().get_store().get(podcast_url).__dict__
            if podcast_dict != None:
                podcast, entry_hashs = migrate_podcast_v0_v1(podcast_dict, strict)
                for entry_id, podpost in entry_hashs.items():
                    if not podpost.id:
                        raise ValueError("Podpost has no id")
                    post_hash_to_id[entry_id] = podpost.id
                    store_post = Factory().get_store().get(entry_id)
                    if store_post:
                        migrate_podpost_v0_v1(store_post.__dict__, podpost)
                    else:
                        logger.debug("Could not find a already persisted post object for %s, reading it from feed",
                                     entry_id)
            i += 1
            pyotherside.send("migrationProgress", 1, i)
        oldhash_size = len(post_hash_to_id)
        logger.info("sizes: hashdict %d podposts sqlite: %d", oldhash_size, Podpost.select().count())
        post_hash_to_id = migrate_all_podposts_from_v0(post_hash_to_id)
        if len(post_hash_to_id) < oldhash_size:
            raise ValueError("Got only %d hashs, but expected at least %d" % (len(post_hash_to_id), oldhash_size))
        migrate_archive_v0_v1(post_hash_to_id)
        migrate_queue_v0_v1(post_hash_to_id)
        migrate_inbox_v0_v1(post_hash_to_id)
        migrate_external_v0_v1(post_hash_to_id)
        set_versionnumber(1)

    if current_version < 2 and start_version == 1:
        pyotherside.send("migrationStart", 2, 4)
        from playhouse.migrate import SqliteMigrator
        migrator = SqliteMigrator(db)
        logger.info("migration2: Starting migration of the datamodel...")
        db.create_tables([LogMessage])
        db.pragma("foreign_keys", "off")
        recreate_table(Podpost, "podpost")
        pyotherside.send("migrationProgress", 2, 1)
        recreate_table(PodpostChapter, "podpostchapter")
        pyotherside.send("migrationProgress", 2, 2)
        recreate_table(ArchiveEntry, "archiveentry")
        pyotherside.send("migrationProgress", 2, 3)
        recreate_table(InboxEntry, "inboxentry")
        pyotherside.send("migrationProgress", 2, 4)
        migrate(
            migrator.add_index("podcast", Podcast.url.column_name)
        )
        db.pragma("foreign_keys", "on")
        set_versionnumber(2)
    if current_version < 3 and start_version > 1:
        pickle_count = Factory().get_store().count()
        logger.info("num pickles for migration to v3: %d", pickle_count)
        pyotherside.send("migrationStart", 3, pickle_count)
        try:
            migrate_all_podposts_from_v0({})
        except:
            if strict:
                raise
        set_versionnumber(3)
    if current_version < 4 and start_version > 1:
        # duration is now in ms
        db.execute_sql("UPDATE podpost set duration = duration * 1000;")
        set_versionnumber(4)
    if current_version < 5 and start_version > 1:
        logger.info("Creating QueueData table on %s/%s", db.database, QueueData._meta.database)
        db.create_tables([QueueData])
        time.sleep(0.1)
        logger.info("Testing queuetable access")
        QueueData.get_queue()
        old_podposts = get_queue_from_pickle_file().podposts
        logger.info("Got queue %s", old_podposts)
        if old_podposts:
            queue = QueueFactory().get_queue()
            queue.podposts = old_podposts
            queue.sanitize()
            queue.save()
        set_versionnumber(5)
    if current_version < 6 and start_version > 1:
        # the fields were filled with the podcast logo path, we need that for the episode logo now
        logger.info("clearing podpost logo data")
        db.execute_sql("UPDATE podpost set logo_url = null;")
        db.execute_sql("UPDATE podpost set logo_path = null;")
        set_versionnumber(6)
    db.execute_sql('VACUUM "main"')
    pyotherside.send("migrationDone")


def get_queue_from_pickle_file():
    return Factory().get_store().get("the_queue")


def recreate_table(table: Type[Model], tablename: str):
    logger.info("copying table %s", tablename)
    table_copy_name = tablename + "_x"
    col_names = ",".join(col.name for col in db.get_columns(tablename))
    db.execute_sql("ALTER TABLE %s RENAME TO %s;" % (tablename, table_copy_name))
    db.create_tables([table])
    sql = "INSERT INTO %s (%s) SELECT %s FROM %s;" % (tablename, col_names, col_names, table_copy_name)
    # todo: check for returned sql errors
    db.execute_sql(sql)
    logger.info("deleting copy of table %s", tablename)
    db.execute_sql("DROP TABLE %s_x;" % tablename)


def get_archive_v0_count():
    archive = get_archive_from_v0_store()
    if archive is None:
        return 0
    return len(archive.__dict__['podposts'])


def get_podcastlist_from_pickles():
    plist = Factory().get_store().get(listname)
    if not plist:
        logger.warning("No podcastlist for migration found")
        return []
    return plist.podcasts


def get_versionnumber() -> int:
    versionfilepath = get_versionfile_path()
    if os.path.isfile(versionfilepath):
        with open(versionfilepath, 'r') as versionfile:
            return int(versionfile.read())
    else:
        return 0


def set_versionnumber(number: int):
    versionfilepath = get_versionfile_path()
    with open(versionfilepath, 'w') as versionfile:
        versionfile.write(str(number))


def get_versionfile_path():
    return os.path.join(Constants().data_home, "dataversion")


def setup_db():
    from .podpost import Podpost, PodpostChapter
    logger.info("creating db tables. db: %s", db.database)
    models = [Podpost, PodpostChapter, Podcast, ArchiveEntry, InboxEntry, ExternalEntry, LogMessage, QueueData]
    db.bind(models)
    db.create_tables(models)
    import time
    counter: int = 0
    while len(db.get_tables()) != len(models):
        if counter > 50:
            raise Exception("timed out while waiting on table creation")
        counter += 1
        logger.info("Waiting for tables to be created")
        time.sleep(0.1)
    logger.info("created db tables: %s", db.get_tables())


def migrate_archive_v0_v1(post_hash_to_id, progress_callback=None):
    """
    migrates the id list to ArchiveEntry
    """
    from podcast.archive import Archive
    archive = get_archive_from_v0_store()
    if archive is None:
        return
    podposts = archive.__dict__['podposts']
    if len(podposts) > 0:
        old_archive: List[str] = podposts
        Archive().bulk_insert(list(post_hash_to_id[hash] for hash in old_archive if hash in post_hash_to_id.keys()))
    else:
        logger.warning("Could not get old archive for migration")


def __get_existing_posts_href_cache():
    return set(t[0] for t in Podpost.select(Podpost.href).tuples())


def __validate_store_entry(known_post, podcast):
    if known_post['podurl'] and not podcast:
        logger.warning("Found archiveentry with unknown podcast %s", known_post['podurl'])
        return False
    if not "href" in known_post.keys():
        if not "link" in known_post.keys():
            logger.warning("Cannot migrate podpost from store, missing attributes: '%s'", known_post)
            return False
        known_post["href"] = known_post["link"]
    return True


def __get_podcast_for_entry(known_post, podcasts):
    podcast = podcasts[known_post['podurl']] if known_post['podurl'] in podcasts.keys() else None
    return podcast


def __get_podcast_cache_dict():
    podcasts = {podcast.url: podcast for podcast in podcastlist.PodcastList().get_podcasts_objects()}
    return podcasts


def __get_post_from_store(hash):
    known_entry = Factory().get_store().get(hash)
    if not known_entry:
        logger.warning("Found archiveentry which was not found in a feed without podpost in store")
        return None
    return known_entry.__dict__


def get_archive_from_v0_store():
    from podcast.archive import Archive
    archive: Archive = Factory().get_store().get(archivename)
    if not archive:
        logger.warning("Could not unpickle archive")
    return archive


def create_post_from_store_dict(post_dict, podcast):
    post = Podpost()
    post.podcast = podcast
    post.guid = post_dict["id"]
    for attr in ["isaudio", "position", "logo_url",
                 "logo_path", "insert_date", "published",
                 "title", "link", "href", "plainpart",
                 "htmlpart", "author", "length", "type", "duration"]:
        transfer_attribute(attr, post_dict, post)
    if post.insert_date is None:
        post.insert_date = datetime.datetime.now().timestamp()
    migrate_podpost_v0_v1(post_dict, post, do_persist=False)
    return post


def migrate_podpost_v0_v1(post_dict: dict, podpost: Podpost, do_persist=True):
    if not podpost:
        raise ValueError("podpost must not be none")
    for attr in ["favorite", "file_path", "percentage", "position", "state"]:
        transfer_attribute(attr, post_dict, podpost)
    if do_persist:
        PodpostFactory().persist(podpost)


def transfer_attribute(which: str, post_dict: dict, podpost: Podpost):
    if which in post_dict:
        setattr(podpost, which, post_dict[which])


def migrate_all_podposts_from_v0(post_hash_to_id):
    href_to_hash = {}
    invalid_posts = duplicated_hrefs = 0

    def inner_iterator():
        nonlocal href_to_hash, invalid_posts, duplicated_hrefs
        try:
            podcast_cache = __get_podcast_cache_dict()
            hrefs = __get_existing_posts_href_cache()
            for podpost_dict, hash in Factory().get_store().iter(["href", "title", "podurl"]):
                try:
                    podcast = __get_podcast_for_entry(podpost_dict, podcast_cache)
                    if __validate_store_entry(podpost_dict, podcast):
                        href = podpost_dict["href"]
                        if href not in hrefs and href not in href_to_hash.keys():
                            yield create_post_from_store_dict(podpost_dict, podcast)
                        else:
                            duplicated_hrefs += 1
                        href_to_hash[href] = hash
                    else:
                        invalid_posts += 1
                except:
                    logger.exception("Error loading post")
                    invalid_posts += 1
        except:
            logger.exception("general error with iterating the store")
            raise

    if Factory().get_store().count() <= 1:
        return

    for offset, total, chunk in chunks(inner_iterator(), 50):
        Podpost.bulk_create(chunk)
        logger.info("migrated podposts items %d", total)
    logger.info("collected %d hrefs, invalid %d, duplicated %d", len(href_to_hash), invalid_posts, duplicated_hrefs)
    for id, href in Podpost.select(Podpost.id, Podpost.href).tuples():
        if href in href_to_hash.keys():
            post_hash_to_id[href_to_hash[href]] = id
    return post_hash_to_id


def migrate_podcast_v0_v1(podcast_dict: dict, strict=True) -> Tuple[Podcast, Dict[str, Podpost]]:
    logger.info("migrating podcast '%s' to new format version 1 strictmode: %s", podcast_dict['title'], strict)
    try:
        podcast: Podcast = Podcast(**podcast_dict)
        PodcastFactory().persist(podcast)
        entry_ids_new_to_old = []
        old_entry_count = 0
        feed = podcast_dict['feed']
        entry_hashs: Dict[str, Podpost] = {}
        chaptercount = 0
        if feed is not None:
            old_entry_count = len(feed.entries)
            logger.info("Found %d entries in old format", old_entry_count)
            for entry in feed.entries:
                try:
                    post = Podpost.from_feed_entry(podcast, entry, podcast_dict['url'])
                except Exception as e:
                    logger.exception("could not load post from podcast %s", podcast_dict['url'])
                    if strict:
                        raise e
                    else:
                        continue
                p2 = PodpostFactory().get_podpost(post.id)
                if p2:
                    post = p2
                chaptercount += len(post.chapters)
                PodpostFactory().persist(post, store_chapters=True)
                hash_id: str
                if "id" in entry:
                    if entry.id != "":
                        hash_id = hashlib.sha256(entry.id.encode()).hexdigest()
                    else:
                        hash_id = hashlib.sha256(entry.summary.encode()).hexdigest()
                else:
                    hash_id = hashlib.sha256(entry.summary.encode()).hexdigest()
                entry_hashs[hash_id] = post
            podcast._set_alt_feeds(feed)
        else:
            logger.warning("feed for %s was None. Will still continue migration but leave entries empty.",
                           podcast.title)
        logger.info("Found %d entries in new format", len(podcast.entry_ids_old_to_new))
        if strict:
            assert chaptercount == PodpostChapter.select().join(Podpost).where(Podpost.podcast == podcast).count()
        if not strict or old_entry_count == len(podcast.entry_ids_old_to_new):
            logger.info("migration done")
            PodcastFactory().persist(podcast)
            return podcast, entry_hashs
        else:
            logger.error("Could not complete migration. old count: %d, new: %d", old_entry_count,
                         len(entry_ids_new_to_old))
            raise ValueError("Migration failed. Expected %d entries, but only migrated %d", old_entry_count,
                             len(podcast.entry_ids_old_to_new))
    except Exception as e:
        logger.exception("error while migrating podcast %s to new format. strictmode: %s", podcast_dict['title'],
                         strict)
        raise e


def migrate_queue_v0_v1(post_hash_to_id):
    QueueFactory().get_queue().podposts = [post_hash_to_id[hash] for hash in get_queue_from_pickle_file().podposts if
                                           hash in post_hash_to_id]
    QueueFactory().get_queue().save()


def migrate_inbox_v0_v1(post_hash_to_id):
    inbox_pickle = Factory().get_store().get(inboxname)
    if not inbox_pickle:
        logger.warning("could not load inbox pickle")
        return
    inbox_list = inbox_pickle.__dict__["podposts"]
    for hash in inbox_list:
        if hash in post_hash_to_id:
            Inbox().insert(post_hash_to_id[hash])
        else:
            logger.error("Had unknown post %s in inbox", hash)


def migrate_external_v0_v1(post_hash_to_id):
    external_obj = Factory().get_store().get(externalname)
    if not external_obj or 'podposts' not in external_obj.__dict__:
        logger.warning("Could not load external pickle")
        return
    external = ExternalFactory().get_external()
    for hash in external_obj.__dict__['podposts']:
        if hash in post_hash_to_id:
            post: Podpost = post_hash_to_id[hash]
            external.insert(post, post.file_path)
        else:
            logger.error("Had unknown post %s in external", hash)
